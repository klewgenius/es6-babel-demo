import Person from '../model/Person';

export default class Child extends Person { //inheritance
    constructor(name, lastName, old) {
        super(name, lastName); //call the parent constructor with super
        this.old = old;
    }

    get fullName() {
        return super.fullName + " - " + this.old + " years old.";
    }

}