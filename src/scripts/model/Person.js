/*
  Model classes can be exported and imported directly (not using AngularJS' dependency injection).
  export Modules!
*/


export default class Person {

	constructor(name, lastName) {

		this.name = name;
		this.lastName = lastName;
	}

	get fullName() {
		return `My name is ${this.name}, and my last name is ${this.lastName}?`;// this.name + ' - ' + this.lastName;
	}

}