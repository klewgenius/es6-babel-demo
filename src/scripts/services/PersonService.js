import Person from '../model/Person';
import Child from '../model/Child';

export default class PersonService {

	/*
	  The below annotation will be processes by ngAnnotate, which
	  will annotate the constructor after compiling for minification.
	*/

	/*@ngInject;*/
	constructor() {
	}

	getPerson() {
		let person = new Person('Andres', 'Baez');//this._$q.when(new Person('Andres'));
		return person.fullName;
	}
	getChild() {
		let person = new Child('Andres', 'Baez', 12);//this._$q.when(new Person('Andres'));
		return person.fullName;
	}
}